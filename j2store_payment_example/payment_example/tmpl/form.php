<?php
/*
* @package		Example payment plugin for j2store
* @subpackage	J2Store
* @author    	John doe
* @copyright	Copyright (c) 2015 XYZ company. All rights reserved.
* @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
* --------------------------------------------------------------------------------
*/

defined('_JEXEC') or die('Restricted access'); ?>

<p><?php echo 'Information to the customer or the credit card form'; ?></p>
