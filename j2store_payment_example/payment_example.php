<?php
/*
* @package		Example payment plugin for j2store
* @subpackage	J2Store
* @author    	John doe
* @copyright	Copyright (c) 2015 XYZ company. All rights reserved.
* @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
* --------------------------------------------------------------------------------
*/

/** ensure this file is being included by a parent file */
defined('_JEXEC') or die('Restricted access');
require_once (JPATH_ADMINISTRATOR.'/components/com_j2store/library/plugins/payment.php');
class plgJ2StorePayment_example extends J2StorePaymentPlugin
{
	/**
	 * @var $_element  string  Should always correspond with the plugin's filename,
	 *                         forcing it to be unique
	 */
	var $_element    = 'payment_paypal';
	

	/**
	 * Constructor
	 * @param object $subject The object to observe
	 * @param 	array  $config  An array that holds the plugin configuration
	 * @since 1.5
	 */
	function __construct(& $subject, $config) {
		parent::__construct($subject, $config);
		$this->loadLanguage( '', JPATH_ADMINISTRATOR );	
	}
	
	
	/**
	 * Prepares variables for the payment form. 
	 * Displayed when customer selects the method in Shipping and Payment step of Checkout
	 *
	 * @return unknown_type
	 */
	function _renderForm( $data )
	{
		$user = JFactory::getUser();
		$vars = new JObject();
		$vars->onselection_text = 'You have selected this method. You will be redirected.';
		//if this is a direct integration, the form layout should have the credit card form fields.
		$html = $this->_getLayout('form', $vars);

		return $html;
	}

	/**
	 * Method to display a Place order button either to redirect the customer or process the credit card information.
	 * @param $data     array       form post data
	 * @return string   HTML to display
	 */
	function _prePayment( $data )
	{
		// get component params
		$params = J2Store::config();
		$currency = J2Store::currency();

		// prepare the payment form

		$vars = new JObject();
		$vars->order_id = $data['order_id'];
		$vars->orderpayment_id = $data['orderpayment_id'];

		F0FTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_j2store/tables');
		$order = F0FTable::getInstance('Order', 'J2StoreTable')->getClone();
		$order->load(array('order_id'=>$data['order_id']));
		$currency_values= $this->getCurrency($order);

		$vars->currency_code =$currency_values['currency_code'];
		$vars->amount = $currency->format($order->order_total, $currency_values['currency_code'], $currency_values['currency_value'], false);

		$rootURL = rtrim(JURI::base(),'/');
		$subpathURL = JURI::base(true);
		if(!empty($subpathURL) && ($subpathURL != '/')) {
			$rootURL = substr($rootURL, 0, -1 * strlen($subpathURL));
		}
		
		$return_url = $rootURL.JRoute::_("index.php?option=com_j2store&view=checkout&task=confirmPayment&orderpayment_type=".$this->_element."&paction=display");
		$cancel_url = $rootURL.JRoute::_("index.php?option=com_j2store&view=checkout&task=confirmPayment&orderpayment_type=".$this->_element."&paction=cancel");
		$callback_url = JURI::root()."index.php?option=com_j2store&view=checkout&task=confirmPayment&orderpayment_type=".$this->_element."&paction=callback&tmpl=component";
		
		$orderinfo = $order->getOrderInformation();

		$vars->invoice = $order->getInvoiceNumber();

		$html = $this->_getLayout('prepayment', $vars);
		return $html;
	}

	/**
	 * Processes the payment form
	 * and returns HTML to be displayed to the user
	 * generally with a success/failed message
	 *
	 * @param $data     array       form post data
	 * @return string   HTML to display
	 */
	function _postPayment( $data )
	{
		// Process the payment
		$app = JFactory::getApplication();
		$paction = $app ->input->getString('paction');

		$vars = new JObject();

		switch ($paction)
		{
			case "display":
				$vars->message = 'Thank you for the order.';
				$html = $this->_getLayout('message', $vars);
				//get the thank you message from the article (ID) provided in the plugin params
				$html .= $this->_displayArticle();
				break;
			case "callback":
				//Its a call back. You can update the order based on the response from the payment gateway
				$vars->message = 'Some message to the gateway';
				$html = $this->_getLayout('message', $vars);
				echo $html; 
				$app->close();
				break;
			case "cancel":
				//cancel is called. 
				$vars->message = 'Sorry, you have cancelled the order';
				$html = $this->_getLayout('message', $vars);
				break;
			default:
				$vars->message = 'Seems an unknow request.';
				$html = $this->_getLayout('message', $vars);
				break;
		}

		return $html;
	}

}
